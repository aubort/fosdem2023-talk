FOSDEM & CfgMgmtCamp 2023
=========================

Installation
------------

```
apt install texlive-latex-extra pandoc impressive
```

Build
-----

```
make
make view
```
