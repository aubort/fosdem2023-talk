% FOSDEM and CfgMgmtCamp 2023
% Nicolas Coudene, Pablo Llopis && Jean-Baptiste Aubort; SCITAS
% \today
---
theme: focus
fontsize: 8pt
---

# FOSDEM

![](media/logo.png)

    Open Source and Free Software conference since 2000.

**Location**: Brussels, Belgium. **Date**: 4-5th of February 2023

## Numberphile

* 8000+ attendees, 771 lectures
* 380 hours of video content, 787 speakers, up to 35 concurent streams
* 53'130  ipv6 addresses, top website: github.com
* 2500 t-shirts and 500 hoodies sold
* 1500 pizzas, 600 sandwiches, 750kg fries, 3200 waffles, ...

# FOSDEM

![](media/azimuth.png)

K8s, OpenStack and RDMA are just like oil, vinegar and bread?

## Self-service Kubernetes Platforms with RDMA on OpenStack

* Using Open OnDemand to provide services in the cloud
    * Slurm, k8s, JupyterHub, VMs
* LOKI -> Linux, OpenStack, Kubernetes Infrastructure
    * RDMA inside OpenStack (PCI passthrough)
    * K8s on OpenStack (CAPI, Helm charts for deploying k8s using Cluster API)
    * RDMA Inside K8s pods
* [Slides link](https://fosdem.org/2023/schedule/event/k8s_rdma_openstack/attachments/slides/5807/export/events/attachments/k8s_rdma_openstack/slides/5807/openstack_k8s_rdma.pdf)

# FOSDEM

![](media/cleantest.png)

## Developing effective testing pipelines for HPC applications

* Provision test environment on containers (lxd)
* Using python to define and run tests
* Manage test results and reports
* Only supports apt/snap/systemd/exec
* -> Pretty basic but interesting concepts
* [Video link](https://video.fosdem.org/2023/UD2.120%20(Chavanne)/hpc_effective_testing_pipelines.mp4)

# FOSDEM

![](media/grotte.png)

## Grottocenter: An open source database for cavers

* It contains information about caves that can be used to prepare a caving trip but also background information that can be useful to learn more about the geology in a specific region.
* Wiki style, open access, machine readable
* [Video link](https://video.fosdem.org/2023/H.2215%20(Ferrer)/grottocenter.mp4)

# FOSDEM

![](media/kiri-moto.png)

## Maker Tools in the Browser: CAM to 3D Printing: Zero Install, Always Up to Date

* Browser based 3d printing tools
    * Slicer: Kiri:Moto
    * Mesh repair: Mesh:Tool
* [Video link](https://video.fosdem.org/2023/K.1.105%20(La%20Fontaine)/browser_maker_tools.mp4)

# FOSDEM

![](media/matrix.png)

## Matrix 2.0

* Matrix is an open standard for secure, decentralised communication
* Rework the slowest bits of Matrix
* By far the biggest change to Matrix since the project began in 2014
* [Video link](https://video.fosdem.org/2023/Janson/matrix20.mp4)

# FOSDEM

![](media/nasa.png)

## Open Source Software at NASA

* NASA intensively uses open source software
* The talk starts with typical NASA propagenda ;) (it's also their role hehe)
* Some OSS example:
    * Apollo Guidance Computer has been opensourced years ago: https://github.com/chrislgarry/Apollo-11 and you can find emulator to run this code https://www.ibiblio.org/apollo/
    * JWST calibration software https://github.com/spacetelescope/jwst
* [Video link](https://video.fosdem.org/2023/Janson/nasa.mp4)
* [Slides link](https://fosdem.org/2023/schedule/event/nasa/attachments/slides/5829/export/events/attachments/nasa/slides/5829/FOSEM_NASA_OSS.pdf)

# FOSDEM

## Open Source Software at NASA

![](media/nasa1.png)

# FOSDEM

## Open Source Software at NASA

![](media/nasa2.png)

# FOSDEM

## Things missed which are probably interesting

* Graphics: A Frame's Journey
* Is OpenStack still needed in 2023 ?
* Safer containers through system call interception
* Making Continuous Delivery Accessible to All
* IntelOwl Project
* Automatic secret rotation in Kubernetes
* CI/CD The gitops way
* Zero Knowledge Cryptography and Anonymous Engineering
* Controlling the web with a PS5 controller
* 7 things I learned about old computers, via emulation
* If it's public money, make it public code!

# CfgMgmtCamp

![](media/cfgmgmtcamp.png)

    Configuration Management Camp is the event for technologists interested in
    Open Source Infrastructure automation and related topics. This includes but
    is not limited to: Open Source Configuration Management, Provisioning,
    Orchestration, Choreography, Container Operations, and many more topics.

**Location**: Ghent, Belgium. **Date**: 6-8th of February 2023

**Rooms**: Terraform, Ansible, Rudder, Observability, MgmtConfig, Puppet, Pulumi, Foreman, CFEngine, Pulp, Security, ...

# CfgMgmtCamp

![](media/feature-branch.png)

## Feature Branching is Evil

* Problems
    * Feature branch hides work for the rest of the team
    * Feature branch make one work in isolation, for a long time,
      the individual goes faster but the team is not, review and ci are usually
      blocked away -> continuous isolation
    * Merging = team communication
    * Feature branch discourage refactoring,
      it's harder to do as there's more potential conflicts to handle
* Some fixes/ideas
    * Main branch should be production ready
    * Lots of fast tests on almost every commit
    * Feature toggle in main branch, even if feature is incomplete it's merged
      often and not activated by default but only with a --flag/option/option
    * Pair programming instead of after the fact code review
* [Slides link](https://cfp.cfgmgmtcamp.org/media/2023/submissions/YCVPYG/resources/2023_CfgMgmtCamp_-_Feature_Branching_considered_Evil_VEgrEgc.pdf)

# CfgMgmtCamp

![](media/puppet.jpg)

## Puppet room

* Not very interesting, lots of old people talking about the same thing

# FOSDEM flu™

![Infected FOSDEM logo](media/fosdem-flu.png)


## It was very strong this year

* Out of the 10 people in the team we were in, half of us got the FOSDEM flu
* The flu was very strong this year, a week of sickness

# Beers

![Beers, lots of beers](media/beers.jpg){ width=100% }

## select * from beers order by type

    Name              Rating    ABV     Type
    Tripel Karmeliet  4.50 /5   8.4%    Belgian Tripel
    Tripel LeFort     4.00 /5   8.8%    Belgian Tripel
    Curieuse Neus     3.00 /5   7.0%    Belgian Tripel
    Moinette Blonde   4.00 /5   8.5%    Belgian Strong Golden Ale
    Principale Bruin  3.75 /5   9.0%    Belgian Strong Dark Ale
    Bolleke           2.00 /5   5.0%    Belgian Pale Ale
    Troubadour Magma  2.75 /5   9.0%    Belgian IPA
    Bonnie And Clyde  2.00 /5   9.6%    IPA Imperial Double
    Brugge Tripel     3.50 /5   8.7%    Ancient Herbed Ale
