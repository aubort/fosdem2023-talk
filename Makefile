SHELL := /bin/bash

all:
	pandoc -t beamer -s presentation.md -o presentation.pdf
	xdg-open presentation.pdf

view: presentation.pdf
	impressive -c memory --nologo --windowed -g 1680x1050 presentation.pdf
